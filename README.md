## A Non-Intrusive Method to Inferring Linear Port-Hamiltonian Realizations using Time-Domain Data

This repository contains the MATLAB implementation for constructing linear port-Hamiltonian systems from time-domain input-output data. This method proposed in [4] is a non-intrusive method that constructs port-Hamiltonian realizations based on inferred frequency response data from time-domain data.
The resulting LTI port Hamiltonian system has the form:
<center>
<p align="center"><img src="./Img/pHsys.png" 
alt="$\dot{\hat {\mathbf{xxxxxxxx}}} = \hat{\mathbf{A}}\hat{\mathbf{x}} + \hat{\mathbf{H}}(\hat{\mathbf{x}}\otimes\hat{\mathbf{x}}) + \hat{\mathbf{B}}\mathbf{u}(t) + \sum_{i=1}^m\hat{\mathbf{N}}\hat{\mathbf{x}}u_i(t) + \hat{\mathbf{C}}$"
 height=60/></p>
 </center>
where J,R,G,... satisfy certain constraints detailed in [4]. These define the structure of a port-Hamiltonian system. This structure is obtained by this proposed algorithm. For more details about the theory, check the paper [4].

## Overview of the algorithm
These are the main steps for the time domain port-Hamiltonian realization:

* Input/Output time-domain data are collected.
* The Time domain data is used to infer frequency domain data based on the Loewner framework [1] using a technique proposed in [2]. 
* The inferred frequency domain data is used to compute spectral zeros and zero directions that are used to construct the port Hamiltonian realization by following the algorithm presented in [3].


## Main Function
```
[pHModel] = time_pHModel( uInF, yT, deltaT, s, sInterpIndx, params)
```
***Arguments***

* `uInF` 	            - Input vector
* `yT` 	                - Output vector
* `deltaT` 	            - Time sampling rate
* `s` 	                - Sampling points
* `sInterpIndx`         - Interpolation points
* `params.kminPercent` 	- Percent of points used in reg problem
* `params.dim` 	        - Dimension of discrete time learned model. 

***Return values***

* `pHModel`             - Matrices of the identified port-Hamiltonian system

Note that the order of the resulting pH system might be less than `params.dim` if the order of the minimal realization of the pH system is lower than `params.dim`.
## Examples 
The `Examples` folder contains the two examples considered in the paper [4] reproducing the same results shown in [4].

### RLC circuit 
This example is an RLC ladder circuit.  This example has three parts:
* `Norm_RLC.m` This script computes the H2 and Hinf norms for different system dimensions. It returns two figures: the first one compares H2 norm in terms of the order of the realization. The second compares the Hinf error.
* `Example_RLC.m` This script computes a realization for a fixed realization order. It also returns two figures: the first one compares the Bode plots of the original and the port-Hamiltonian systems and the second shows the time response comparison of these systems.
* `Noise_RLC.m` This script computes the H2 and Hinf norms for data corrupted with Gaussian white noise. It returns the H2 and Hinf error norms in terms of standard deviation of the Gaussian white noise.

### Spiral example 
This example is a proximity sensor constituted of a spiral inductor and a plane of copper on top of the spiral.  This example has three parts:
* `Norm_Spiral.m` This script computes the H2 and Hinf norms for different system dimensions. It returns two figures: the first one compares H2 norm in terms of the order of the realization. The second compares the Hinf error.
* `Example_Spiral.m` This script computes a realization for a fixed realization order. It also returns two figures: the first one compares the Bode plots of the original and the port-Hamiltonian systems and the second shows the time response comparison of these systems.
* `Noise_Spiral.m` This script computes the H2 and Hinf norms for data corrupted with Gaussian white noise. It returns the H2 and Hinf error norms in terms of standard deviation of the Gaussian white noise.


## Citation
Please cite the paper [4] if you use the function `time_pHModel` in your research work.

## References
[1]. Mayo A. J., and Antoulas A. C., A framework for the solution of the generalized realization problem, Linear Algebra Appl. 425: 634–662, 2007.

[2]. Peherstorfer B., Gugercin S., and Willcox K., Data-driven reduced model construction with time-domain Loewner models. SIAM J. Sci. Comput., 39:A2152–A2178, 2017.

[3]. Benner P., Goyal P., and Van Dooren P., Identification of port-Hamiltonian systems from frequency response data, arXiv:1911.00080, 2019.

[4]. Cherifi K., Goyal P., and Benner P., A non-intrusive method to inferring linear port-Hamiltonian realizations using time-domain data, 2020.
 
## Contact
Please contact [Karim Cherifi](mailto:cherifi@mpi-magdeburg.mpg.de) for any queries and comments. 
___

This script has been written in MATLAB 2016b. I would like to thank Benjamin Peherstorfer, and Pawan Goyal for providing their MATLAB implementations.
