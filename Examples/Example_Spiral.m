%% non intrusive time domain PH realization: Oseen example
% This example in section Example 4.A in the paper
%
% Cherifi K., Goyal, P., and Benner, P., A Non-Intrusive Method to Inferring 
% Linear Port-Hamiltonian Realizations using Time-Domain Data, 2020.
%
% -------------------------------------------------------------------------
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) 2019-2020 Karim Cherifi, Pawan Goyal, Peter Benner
% Contact: Karim Cherifi, cherifi@mpi-magdeburg.mpg.de
% -------------------------------------------------------------------------

% Load Oseen example and set parameters
addpath(genpath('../Functions/'));

clearvars
clc
close all

Tend        = 1e2;        % end time
deltaT      = 1e-2;      % time step
dim         = 5; %5; %20;         % dimension of discrete time learned model
m           = 100;        % number of interpolation points
kminPercent = 75;         % skip the first 25% of time steps

load('spiral.mat'); %load spiral example as (A,B,C,E) system
origsys.A=A; origsys.B=B; origsys.C=C; origsys.E=E;


%% Systen simulation and data generation

% Data preparation and system simulation
t = 0:deltaT:Tend-deltaT; %time steps

% potential interpolation points
s = 2*pi*1j*(-length(t)/2:(length(t)/2- 1))/length(t);

% select interpolation points in this range
minFreq = 2*pi*1e-03;
maxFreq = 3;

% get input signal
u  =  getInputSignal(t, s, m, minFreq, maxFreq);

%select interpolation points from the reange of frequencies s
sInterpIndx = selectImportantModes(u, m, s, minFreq, maxFreq, 'equidistantConjugate');

uInF  =                 fftshift(fft(u));
uTInF =                 zeros(size(uInF));
uTInF(sInterpIndx) =    uInF(sInterpIndx);
uT =                    ifft(ifftshift(uTInF)); % 'truncated' 

% compute the discrete system and its time domain simulation
[yT, Ad, Bd, Cd, Ed] = bwdEuler(u, t, 1e-7*origsys.A, 1e-7*origsys.B, origsys.C, origsys.E); 

%% computation of the realization using time Loewner
tStartLoew = tic;

% Compute a Loewner realization from time domain data
params.kminPercent =   kminPercent;
params.dim         =   dim;
params.tol         =   1e-8 ;
params.tolLoew     =   1e-8 ;

 
[pH_Loew]   = time_pHModel( uInF, yT,deltaT, s, sInterpIndx,params);
Order_Loew  = size(pH_Loew.A,1);

pH_Loew.A  =   1e7*pH_Loew.A;
pH_Loew.B  =   1e7*pH_Loew.B;

tEndLoew = toc(tStartLoew); % computation time for Loewner method


%% computation of the realization for MOESP
tStart_MOESP = tic; 

data_MOESP= iddata({real(yT)',imag(yT)'},{real(u)',imag(u)'},deltaT); % Form data 

opt_MOESP = ssestOptions('N4Weight','MOESP'); % Choose MOESP method

% Compute the discrete tine realization of the system using MOESP
Ident_MOESP   = ssest(data_MOESP,dim,'Feedthrough',0,'DisturbanceModel','none','Ts',deltaT,opt_MOESP); 

% Get a pH realization
tol_MOESP   =   1e-8 ;
[pH_MOESP,out_MOESP,Order_MOESP] = d2pH(Ident_MOESP,deltaT,tol_MOESP);

pH_MOESP.A  =   1e7*pH_MOESP.A;
pH_MOESP.B  =   1e7*pH_MOESP.B;

tEnd_MOESP = toc(tStart_MOESP);  % computation time for MOESP

%% computation of the realization for CVA
tStart_CVA = tic; 

data_CVA = iddata({real(yT)',imag(yT)'},{real(u)',imag(u)'},deltaT); % Form data

opt_CVA = ssestOptions('N4Weight','CVA'); % Choose CVA method

% Compute the discrete tine realization of the system using CVA
Ident_CVA   = ssest(data_CVA,dim,'Feedthrough',0,'DisturbanceModel','none','Ts',deltaT,opt_CVA);

% Get a pH realization
tol_CVA   =   1e-8 ;
[pH_CVA,out_CVA,Order_CVA] = d2pH(Ident_CVA,deltaT,tol_CVA);

pH_CVA.A  =   1e7*pH_CVA.A;
pH_CVA.B  =   1e7*pH_CVA.B;

tEnd_CVA = toc(tStart_CVA); % computation time for CVA

%% compute norms
%{
sys_orig    = dss(origsys.A,origsys.B,origsys.C,0,origsys.E);
sys_Loew    = dss(pH_Loew.A,pH_Loew.B,pH_Loew.C,0,pH_Loew.E);
sys_MOESP   = dss(pH_MOESP.A,pH_MOESP.B,pH_MOESP.C,0,pH_MOESP.E);
sys_CVA     = dss(pH_CVA.A,pH_CVA.B,pH_CVA.C,0,pH_CVA.E);

H2Norm.Loew     = norm(sys_Loew-sys_orig );
H2Norm.MOESP    = norm(sys_MOESP-sys_orig );
H2Norm.CVA      = norm(sys_CVA -sys_orig );

HinfNorm.Loew   = hinfnorm(sys_Loew-sys_orig );
HinfNorm.MOESP  = hinfnorm(sys_MOESP-sys_orig );
HinfNorm.CVA    = hinfnorm(sys_CVA -sys_orig );
%}

%% Compare the transfer functions of the original and pH systems
tikzflag = 0 ;

H_orig   =  @(s) origsys.C*((s*origsys.E-origsys.A)\origsys.B) ; 
H_Loew   =  @(s) pH_Loew.C*((s*pH_Loew.E-pH_Loew.A)\pH_Loew.B) + pH_Loew.D; 
H_MOESP  =  @(s) pH_MOESP.C*((s*pH_MOESP.E-pH_MOESP.A)\pH_MOESP.B) + pH_MOESP.D; 
H_CVA    =  @(s) pH_CVA.C*((s*pH_CVA.E-pH_CVA.A)\pH_CVA.B) + pH_CVA.D; 

w       = logspace(4,12,100);%logspace(4,10,50);

sH_orig  =  zeros(1,length(w));
sH_Loew  =  zeros(1,length(w));
sH_MOESP =  zeros(1,length(w));
sH_CVA   =  zeros(1,length(w));
for j = 1:length(w)
    sH_orig(j)  = H_orig(w(j)*1i);
    sH_Loew(j)  = H_Loew(w(j)*1i);
    sH_MOESP(j) = H_MOESP(w(j)*1i);
    sH_CVA(j)   = H_CVA(w(j)*1i);
end

% plot the bode plot for the original and identified pH model and the error
%between them.
figure(1)

subplot(1,2,1)
loglog(w,abs(sH_orig),'*r',w,abs(sH_Loew),'b',w,abs(sH_MOESP),'--g',w,abs(sH_CVA),'-.k')
legend('Original system','Loewner appproach','MOESP approach','CVA approach','Location','Best')

subplot(1,2,2)
loglog(w,abs(sH_orig-sH_Loew),'b',w,abs(sH_orig-sH_MOESP),'--g',w,abs(sH_orig-sH_CVA),'-.k')
legend('Loewner approach','MOESP approach','CVA approach','Location','Best')


if tikzflag == 1
addpath('M2tikz/')
matlab2tikz('Exm_Spiral_TF.tikz','width','\fwidth','height','\fheight')
end

%plot step reponse of the original and identified pH model
%ttest  = 0:10^-8:10^-6;
%ttest   =  0:0.1:20;
%ttest  = 0:10^-7:10^-5;
ttest  = 0:10^-10:2*10^-8;

%u       =  sin(ttest)+sin(2*ttest)+sin(0.5*ttest);
%u       =  10*(sin(ttest)+sin(4*ttest)+sin(0.25*ttest)).*exp(-0.1*ttest);
%u       =  10*(sin(ttest)+sin(100*ttest)+sin(0.25*ttest)).*exp(-0.1*ttest);
u       =  10*(sin(ttest)+sin(10^9*ttest)+sin(0.25*ttest)).*exp(-0.1*ttest);

dss_orig  =  dss(origsys.A,origsys.B,origsys.C,0,origsys.E);
dss_Loew  =  dss(pH_Loew.A,pH_Loew.B,pH_Loew.C,pH_Loew.D,pH_Loew.E);
dss_MOESP =  dss(pH_MOESP.A,pH_MOESP.B,pH_MOESP.C,pH_MOESP.D,pH_MOESP.E);
dss_CVA   =  dss(pH_CVA.A,pH_CVA.B,pH_CVA.C,pH_CVA.D,pH_CVA.E);

y_orig    =  lsim(dss_orig,u,ttest);
y_Loew    =  lsim(dss_Loew,u,ttest);
y_MOESP   =  lsim(dss_MOESP,u,ttest);
y_CVA     =  lsim(dss_CVA,u,ttest);

figure(2)

subplot(1,2,1)
plot(ttest,y_orig,'*r',ttest,y_Loew,'b',ttest,y_MOESP,'--g',ttest,y_CVA,'-.k');
legend('original system','Loewner approach','MOESP approach','CVA approach','Location','Best')

subplot(1,2,2)
semilogy(ttest,abs(y_orig-y_Loew),'b',ttest,abs(y_orig-y_MOESP),'--g',ttest,abs(y_orig-y_CVA),'-.k')
%semilogy(ttest,abs((y_orig-y_Loew)/y_orig),'b',ttest,abs((y_orig-y_MOESP)/y_orig),'--g',ttest,abs((y_orig-y_CVA)/y_orig),'-.k')
legend('Loewner approach','MOESP approach','CVA approach','Location','Best')


if tikzflag == 1
matlab2tikz('Exm_Spiral_time.tikz','width','\fwidth','height','\fheight')
end
