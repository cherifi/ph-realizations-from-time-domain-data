%% non intrusive time domain PH realization: Spiral example with noise
% This example in section Example 4.2 in the paper:
%
% Cherifi K., Goyal, P., and Benner, P., A Non-Intrusive Method to Inferring 
% Linear Port-Hamiltonian Realizations using Time-Domain Data, 2020.
%
% -------------------------------------------------------------------------
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) 2019-2020 Karim Cherifi, Pawan Goyal, Peter Benner
% Contact: Karim Cherifi, cherifi@mpi-magdeburg.mpg.de
% -------------------------------------------------------------------------

%% Load Spiral example and set parameters
addpath(genpath('../Functions/'));

rng(0)

clearvars
clc
close all

dim         =  4; % choose dimension of the resulting model
Tend        = 100;      % end time
deltaT      = 1e-02;    % sampling time
% set the truncation tolerance
tol.others  = 10^-2;
tol.Loew    = 1e-4 ; 

t  = 0:deltaT:Tend-deltaT; %time steps

% Standard deviation of white noise
Std = [0,10.^(-6:-2)];

Wn_real    =    zeros(length(Std),length(t));
Wn_imag    =    zeros(length(Std),length(t));


% White noise with Standard deviation Std
for i=1:length(Std)
    Wn_real(i,:) = Std(i).*randn(1,length(t));  % Noise for the real part
    Wn_imag(i,:) = Std(i).*randn(1,length(t));  % Noise for the imaginary part
end


load('spiral.mat'); %load Spiral example as (A,B,C,E) system
origsys.A = A; origsys.B = B; origsys.C = C; origsys.E = E; origsys.D = 0;
origsys.A = 1e-7*origsys.A; origsys.B = 1e-7*origsys.B;   % time scaling


%% compute the norms for different standard deviation values
for i=1:length(Std)
    
% Compute the norms for a specific Std value
[H2Norm_temp,HinfNormtemp] =  Norm_Comparison_noisy(origsys,dim,Wn_real(i,:),Wn_imag(i,:),Tend,deltaT,tol);

% H2 norm
H2Norm(1,i)         =      H2Norm_temp.Loew;
H2Norm(2,i)         =      H2Norm_temp.MOESP;
H2Norm(3,i)         =      H2Norm_temp.CVA;

% Hinf norm
HinfNorm(1,i)       =      HinfNormtemp.Loew;
HinfNorm(2,i)       =      HinfNormtemp.MOESP;
HinfNorm(3,i)       =      HinfNormtemp.CVA;

end
