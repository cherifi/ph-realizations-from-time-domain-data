%% non intrusive time domain PH realization: Oseen example
% This example in section Example 4.A in the paper
%
% Cherifi K., Goyal, P., and Benner, P., A Non-Intrusive Method to Inferring 
% Linear Port-Hamiltonian Realizations using Time-Domain Data, 2020.
%
% -------------------------------------------------------------------------
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) 2019-2020 Karim Cherifi, Pawan Goyal, Peter Benner
% Contact: Karim Cherifi, cherifi@mpi-magdeburg.mpg.de
% -------------------------------------------------------------------------

% Load Oseen example and set parameters
addpath(genpath('../Functions/'));

rng(0)

clearvars
clc
close all

Tend        = 150;   % end time
deltaT      = 1e-02;     % time step

t  = 0:deltaT:Tend-deltaT; %time steps


Std = [0,10.^(-6:-2)];

tol.others = 10^-2;
tol.Loew   = 1e-5 ; %1e-5 ; 


Wn_real    =    zeros(length(Std),length(t));
Wn_imag    =    zeros(length(Std),length(t));


for i=1:length(Std)
    Wn_real(i,:) = Std(i).*randn(1,length(t));  % White noise with Standard deviation Std
    Wn_imag(i,:) = Std(i).*randn(1,length(t)); % White noise with Standard deviation Std
end

%load('Noise_RLCSerkan.mat'); % White noise with Standard deviation Std=10^-6

load('rlc_serkan200.mat'); %load RLC example as (A,B,C,E) system
E = eye(size(A));
origsys.A = A; origsys.B = B; origsys.C = C; origsys.E = E; origsys.D = D;

dim = 6;  %7;%4;


for i=1:length(Std)
    
[H2Norm_temp,HinfNormtemp] =  Norm_Comparison_noisy(origsys,dim,Wn_real(i,:),Wn_imag(i,:),Tend,deltaT,tol);

H2Norm(1,i)         =      H2Norm_temp.Loew;
H2Norm(2,i)         =      H2Norm_temp.MOESP;
H2Norm(3,i)         =      H2Norm_temp.CVA;

HinfNorm(1,i)       =      HinfNormtemp.Loew;
HinfNorm(2,i)       =      HinfNormtemp.MOESP;
HinfNorm(3,i)       =      HinfNormtemp.CVA;

end

%% plot H2 and Hinf errors
tikzflag = 0;

%plot the H2 norm with respect to the number of chosen interpolation points
figure(1)
loglog(Std,H2Norm(1,:),'b',Std,H2Norm(2,:),'--g',Std,H2Norm(3,:),'-.k')
legend('H2 error Loewner','H2 error MOESP','H2 error CVA','Location','Best')

if tikzflag == 1
addpath('M2tikz/')
matlab2tikz('H2_Noise_plot_RLCSer.tikz','width','\fwidth','height','\fheight')
end

%plot the Hinf norm with respect to the number of chosen interpolation points
figure(2)
loglog(Std,HinfNorm(1,:),'b',Std,HinfNorm(2,:),'--g',Std,HinfNorm(3,:),'-.k')
legend('Hinf error Loewner','Hinf error MOESP','Hinf error CVA','Location','Best')


if tikzflag == 1
addpath('M2tikz/')
matlab2tikz('Hinf_Noise_plo_RLCSer.tikz','width','\fwidth','height','\fheight')
end
