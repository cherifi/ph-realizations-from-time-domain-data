function     [H2Norm,HinfNorm]= Norm_Comparison_noisy (origsys,dim, Wn_real,Wn_imag,Tend,deltaT,tol)
% -------------------------------------------------------------------------
% This function computes a port-Hamiltonian realization using time domain data
% corrupted with Gaussian white noise for different moethods and compares
% the H2 and Hinf error between them.
% This method was presented in the paper:
% Cherifi K., Goyal, P., and Benner, P., A Non-Intrusive Method to Inferring 
% Linear Port-Hamiltonian Realizations using Time-Domain Data, 2020.
%
% -------------------------------------------------------------------------
% Inputs:
% origsys              ---  Original system,
% dim                  ---  proposed dimension of the learned model,
% Wn_real              ---  Gaussian white noise for the real part of the data,
% Wn_imag              ---  Gaussian white noise for the imaginary part of the data,
% Tend                 ---  Simulation time,
% deltaT               ---  Time sampling rate,
% tol                  ---  Tolerance of learned model.
%
% -------------------------------------------------------------------------
%
% Outputs:
% H2norm              --- H2 norm of the error system between the learned
% models and the original model,
% H2infNorm           --- Hinfinity norm of the error system between the learned
% models and the original model.
% -------------------------------------------------------------------------
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) 2019-2020 Karim Cherifi, Pawan Goyal, Peter Benner
% Contact: Karim Cherifi, cherifi@mpi-magdeburg.mpg.de
% -------------------------------------------------------------------------


% Load Oseen example and set parameters
addpath(genpath('../Functions/'));

m           = 100;    % number of interpolation points
kminPercent = 75;     % skip the first 25% of time steps


%% Systen simulation and data generation

% Data preparation and system simulation
t = 0:deltaT:Tend-deltaT; %time steps

% potential interpolation points
s = 2*pi*1j*(-length(t)/2:(length(t)/2- 1))/length(t);

% select interpolation points in this range
minFreq = 2*pi*1e-03;
maxFreq = 3;

% get input signal
u = getInputSignal(t, s, m, minFreq, maxFreq);

%select interpolation points from the reange of frequencies s
sInterpIndx = selectImportantModes(u, m, s, minFreq, maxFreq, 'equidistantConjugate');

uInF  =                 fftshift(fft(u));
uTInF =                 zeros(size(uInF));
uTInF(sInterpIndx) =    uInF(sInterpIndx);
uT =                    ifft(ifftshift(uTInF)); % 'truncated' 

% compute the discrete system and its time domain simulation
[yT_noN, Ad, Bd, Cd, Ed] = bwdEuler(u, t, origsys.A, origsys.B, origsys.C, origsys.E); 
 

yT = (real(yT_noN) + Wn_real.*(abs(yT_noN))) + 1i *(imag(yT_noN)+ Wn_imag.*(abs(yT_noN)));

%% computation of the realization using time Loewner
tStartLoew = tic;

% Compute a Loewner realization from time domain data
params.kminPercent =   kminPercent;
params.dim         =   dim;
params.tolLoew     =   tol.Loew; 

 
[pH_Loew]   = time_pHModel( uInF, yT,deltaT, s, sInterpIndx,params);
Order_Loew  = size(pH_Loew.A,1);

tEndLoew = toc(tStartLoew); % computation time for Loewner method


%% computation of the realization for MOESP
tStart_MOESP = tic; 

data_MOESP= iddata({real(yT)',imag(yT)'},{real(u)',imag(u)'},deltaT); % Form data 

opt_MOESP = ssestOptions('N4Weight','MOESP'); % Choose MOESP method

% Compute the discrete tine realization of the system using MOESP
Ident_MOESP   = ssest(data_MOESP,dim,'Feedthrough',0,'DisturbanceModel','none','Ts',deltaT,opt_MOESP); 

% Get a pH realization
tol_MOESP   =   tol.others ;
[pH_MOESP,out_MOESP,Order_MOESP] = d2pH(Ident_MOESP,deltaT,tol_MOESP);

tEnd_MOESP = toc(tStart_MOESP);  % computation time for MOESP

%% computation of the realization for CVA
tStart_CVA = tic; 

data_CVA = iddata({real(yT)',imag(yT)'},{real(u)',imag(u)'},deltaT); % Form data

opt_CVA = ssestOptions('N4Weight','CVA'); % Choose CVA method

% Compute the discrete tine realization of the system using CVA
Ident_CVA   = ssest(data_CVA,dim,'Feedthrough',0,'DisturbanceModel','none','Ts',deltaT,opt_CVA);

% Get a pH realization
tol_CVA   =   tol.others ;
[pH_CVA,out_CVA,Order_CVA] = d2pH(Ident_CVA,deltaT,tol_CVA);

tEnd_CVA = toc(tStart_CVA); % computation time for CVA

%% compute norms

sys_orig    = dss(origsys.A,origsys.B,origsys.C,0,origsys.E);
sys_Loew    = dss(pH_Loew.A,pH_Loew.B,pH_Loew.C,0,pH_Loew.E);
sys_MOESP   = dss(pH_MOESP.A,pH_MOESP.B,pH_MOESP.C,0,pH_MOESP.E);
sys_CVA     = dss(pH_CVA.A,pH_CVA.B,pH_CVA.C,0,pH_CVA.E);

% Compute the relative H2 norm
norm2_orig      = norm(sys_orig);
H2Norm.Loew     = norm(sys_Loew-sys_orig)/norm2_orig;
H2Norm.MOESP    = norm(sys_MOESP-sys_orig )/norm2_orig;
H2Norm.CVA      = norm(sys_CVA -sys_orig )/norm2_orig;

% Compute the relative Hinfinity norm
norminf_orig    = hinfnorm(sys_orig); 
HinfNorm.Loew   = hinfnorm(sys_Loew-sys_orig )/norminf_orig;
HinfNorm.MOESP  = hinfnorm(sys_MOESP-sys_orig )/norminf_orig;
HinfNorm.CVA    = hinfnorm(sys_CVA -sys_orig )/norminf_orig;

end
