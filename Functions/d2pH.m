function [pHModel, out, Order_sys] = d2pH(Ident_sys,deltaT,tol)
% -------------------------------------------------------------------------
% This function computes a continous port-Hamiltonian realization from a discrete
% state space realization
% This method was presented in the paper:
% Cherifi K., Goyal, P., and Benner, P., A Non-Intrusive Method to Inferring 
% Linear Port-Hamiltonian Realizations using Time-Domain Data, 2020.
%
% -------------------------------------------------------------------------
% Ident_sys            ---  Discrete identified system,
% deltaT               ---  Time sampling rate,
% tol                  ---  Tolerance of learned model.
% -------------------------------------------------------------------------
%
% Outputs:
% pHModel              --- Matrices of the identified port-Hamiltonian system
% out                  --- Additional informationabout the identified port-Hamiltonian system
% Order_sys            --- Order of the resulting system
%
% -------------------------------------------------------------------------
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) 2019-2020 Karim Cherifi, Pawan Goyal, Peter Benner
% Contact: Karim Cherifi, cherifi@mpi-magdeburg.mpg.de
% -------------------------------------------------------------------------

% discrete to continous transformation based on Backward Euler

clear A B C D E
Er  = eye(size(Ident_sys.A));
A   = (Ident_sys.A-Er)/deltaT;
B   = Ident_sys.B/deltaT;
C   = Ident_sys.C;
D   = 0;
E   = Ident_sys.A;
%}

%{
Er  = eye(size(Ident_sys.A));
sysd=dss(Ident_sys.A,Ident_sys.B,Ident_sys.C,0,Er,deltaT);
sysc = d2c(sysd,'tustin');
%sysc=dss(A,B,C,0,E,deltaT);
[A,B,C,D,E]=dssdata(sysc);
%}


% pH realization

% Define the transfer function for the resulting system
n       =  size(A,1);
H_cont  = @(s) C*((s*E-A)\B) + D;

%define interpolation points used in Contruct_pHModel
Np      = 2*n;

w = logspace(-1,3,Np);
F = zeros(1,Np);

for j = 1:Np
   F(j) = H_cont(1i*w(j));  
end
D = H_cont(10^10);

% compute spectral zeros, spectral directions and deduce the pH representation 
[pHModel, out] = Construct_pHModel(w,F,D,tol);
Order_sys      = size(pHModel.A,1);



end