function [pHModel] = time_pHModel( uInF, yT, deltaT, s, sInterpIndx, params)
% -------------------------------------------------------------------------
% This function computes a port-Hamiltonian realization using time domain data
% This method was presented in the paper:
% Cherifi K., Goyal, P., and Benner, P., A Non-Intrusive Method to Inferring 
% Linear Port-Hamiltonian Realizations using Time-Domain Data, 2020.
%
% -------------------------------------------------------------------------
% Inputs:
% uInF                 ---  input vector,
% yT                   ---  output vector,
% deltaT               ---  Time sampling rate,
% s                    ---  sampling points,
% sInterpIndx          ---  interpolation points,
% params.kminPercent   ---  percent of points used in reg problem,
% params.dim           ---  proposed dimension of the learned model.
%
% Remark: The order of the resulting pH system maybe less than params.dim if the order 
%  of the minimal realization of the pH system is lower than params.dim.
% -------------------------------------------------------------------------
%
% Outputs:
% pHModel              --- Matrices of the identified port-Hamiltonian system
%
% -------------------------------------------------------------------------
%
% This program is free software: you can redistribute it and/or modify
% it under the terms of the GNU Affero General Public License as published
% by the Free Software Foundation, either version 3 of the License, or
% (at your option) any later version.
%
% This program is distributed in the hope that it will be useful,
% but WITHOUT ANY WARRANTY; without even the implied warranty of
% MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
% GNU Affero General Public License for more details.
%
% You should have received a copy of the GNU Affero General Public License
% along with this program.  If not, see <http://www.gnu.org/licenses/>.
%
% Copyright (C) 2019-2020 Karim Cherifi, Pawan Goyal, Peter Benner
% Contact: Karim Cherifi, cherifi@mpi-magdeburg.mpg.de
% -------------------------------------------------------------------------

% default parameters
if  nargin < 6
    params.kminPercent=75;
    params.dim=20;
end

%% interpolate frequency domain data from time domain data
% frequency domain data Hdata corresponding to interpolation points
% sInterpIndx is computed in a least squares problem. RegCond are the
% condition numbers.
[Hdata, ~, regCond] = regProb(params.kminPercent, uInF, yT, s, sInterpIndx);

%% create Loewner (take care of z Transform (discrete!))
%params.dim is the dimension proposed by the user, however this value may
%change if the system is not minimal
tROM = createLoewner(Hdata, exp(s(sInterpIndx)), params.dim);

% store auxiliary data
tROM.sInterpIndx = sInterpIndx;
tROM.sInterp = s(sInterpIndx);
tROM.uInFatInterp = uInF(sInterpIndx);
tROM.regCond = regCond;

%% discrete to continous transformation
% based on Backward Euler
clear A B C D E
A=(tROM.Ar-tROM.Er)/deltaT;
B=tROM.Br/deltaT;
C=-tROM.Cr;
D=0;
E=tROM.Ar;

%% Define the transfer function for the resulting system
n=size(A,1);
% Define the interpolated transfer function 
H_orig  = @(s) C*((s*E-A)\B) + D;

%define interpolation points used in Contruct_pHModel
Np      = 2*n;
w = logspace(-1,3,Np);
F = zeros(1,Np);
for j = 1:Np
   F(j) = H_orig(1i*w(j));  
end
D=H_orig(10^10);

%% pH realization
% compute spectral zeros, spectral directions and deduce the pH representation 
[pHModel, out] = Construct_pHModel(w,F,D);

%% Compute PH framework matrices
S = [-pHModel.A -pHModel.B; pHModel.C pHModel.D];

Vph =(S-S')/2;
Wph =(S+S')/2;

nph=size(pHModel.A,1);

pHModel.J = Vph(1:nph,1:nph);
pHModel.G = Vph(1:nph,nph+1:end);
pHModel.N = Vph(nph+1:end,nph+1:end);
pHModel.R = Wph(1:nph,1:nph);
pHModel.P = Wph(1:nph,nph+1:end);
pHModel.S = Wph(nph+1:end,nph+1:end);
pHModel.Q = inv(pHModel.E);

end