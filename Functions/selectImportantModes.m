function [ I ] = selectImportantModes( u, m, s, minFreq, maxFreq, modeSel )
%SELECTIMPORTANTMODES this function selectes m interpolation points
% between min freq and maxfreq from the given range of frequencies s based 
% on the Selection method modeSel.
% In
%   u               ... input vector
%   m               ... number of interpolation points to be selected
%   s               ... vector containing all possible frequencies
%   minFreq         ... minimum frequency to be selected
%   minFreq         ... maximum frequency to be selected
%   modeSel         ... Selection mode
% Out
%   I               ... vector containing the important modes

%%
if(strcmp(modeSel, 'input'))
    error('Not implemented'); % doesn't ensure complex conjugate pairs are used
    uInF = fft(u);
    [~, I] = sort(abs(uInF), 'descend');
    I = I(1:m);
elseif(strcmp(modeSel, 'equidistant'))
    % select only interpolation points within the test range
    sEqui = s(abs(s) <= abs(maxFreq));
    sEqui = sEqui(abs(sEqui) >= abs(minFreq));
    nrS = length(sEqui);
    I = mylogspaceInteger(nrS, m);
    I = I + sum(abs(s) < abs(minFreq));
elseif(strcmp(modeSel, 'equidistantConjugate'))
    mTotal = length(s);
    sSecondHalf = s(mTotal/2+1:end);
    % select only interpolation points within the test range
    sEqui = sSecondHalf(abs(sSecondHalf) <= abs(maxFreq));
    sEqui = sEqui(abs(sEqui) >= abs(minFreq));
    nrS = length(sEqui);
    I = mylogspaceInteger(nrS, m/2);
    I = I + sum(abs(sSecondHalf) < abs(minFreq));
    
    I = [-((I + mTotal/2) - mTotal/2 - 1 - mTotal/2 - 1), I+mTotal/2];
    Isorted = sort(I, 'ascend');
    I = myReorderS(Isorted);
    %I = Isorted;
    
    % check if we have all complex conjugates
    assert(abs(sum(imag(s(I)))) < 1e-10);
else
    error('Not implemented');
end

end

