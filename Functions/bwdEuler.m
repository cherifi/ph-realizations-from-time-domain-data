function [ y, Ad, Bd, Cd, Ed ] = bwdEuler( u, t, A, B, C, E )
% bwdEuler is a Discretization procedure based on Backward Euler
% In
%   A,B,C,E          ... continous time system
%   u                ... input vector
%   t                ... time samples
% Out
%   Ad,Bd,Cd,Ed      ... Discrete time system
%   y                ... output vector

%%
% initialization
N = size(A, 1);
Tsteps = length(t);
y = zeros(1, Tsteps);
deltaT = t(2) - t(1);


%create discrete system (BwdEuler)
Ed = (E - deltaT*A);
Ad = E;
Bd = deltaT*B;
Cd = C;


%{
% create discrete system (implicit midpoint)
Ed = (E - deltaT*A/2);
Ad = (E+deltaT*A/2);
Bd = deltaT*B;
Cd = C;
%}

% matched
%sysd = c2d(dss(A,B,C,0,E),deltaT,'matched');
%sysd=dss(A,B,C,0,E,deltaT);
%[Ad,Bd,Cd,Dd,Ed]=dssdata(sysd);


% check if impulse/response
if(isempty(u)) % no input given, then impulse response
    x = Bd;
    u = zeros(Tsteps, 1);
else
    x = zeros(N, 1);
end

% simulate the system and compute the output based on the input u
y = timeStep(Ad, Bd, Cd, Ed, u, x); 

end

